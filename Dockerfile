FROM docker.maum.ai:443/brain/vision:cu101

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt --ignore-installed

ENV PYTHONIOENCODING UTF-8

EXPOSE 50300
