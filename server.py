# -*- coding: utf-8 -*-
from __future__ import print_function, division
import os
import cv2
import grpc
import time
import torch
import logging
import argparse
import numpy as np
import pickle as pkl
from concurrent import futures

from data.transform import get_transform
from retrieval import  get_retrieval_result
from utility.utils import load_hparam

from proto.retrieval_pb2 import RetrievalResult
from proto.retrieval_pb2_grpc import add_ImageRetrievalServicer_to_server, ImageRetrievalServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_CHUNK_SIZE = 1024 * 1024


class ImageRetrievalModel:
    def __init__(self, hp):
        self.hp = hp

        if self.hp.extract.model_name.upper() == 'VGG':
            from model.model import Model
            print("Load models")
            self.model = Model(hp)
            self.transform = get_transform(hp, 'test')

        _t = time.time()
        print("Database Loading...")
        self.__get_db()
        print("Database Loaded ! {}".format(time.time() - _t))

    def __get_db(self):
        if self.hp.extract.model_name.upper() in ['HSV', 'LAB', 'RGB', 'VGG']:
            db_name = "{}_value.pkl".format(self.hp.extract.model_name.lower())

            if os.path.exists(db_name):
                with open(db_name, "rb") as f:
                    self.retrieval_db = pkl.load(f)
            else:
                raise AssertionError("Can not found {}\n"
                                     "You have to run update_db.py".format(db_name))
        else:
            raise AssertionError("For Now, Only VGG, HSV, LAB, RGB Supported!!!")


    def get_retrieval(self, img, mask, category_id):
        """
        main retrieval 함수
        :param img: Warped Query image (BGR)
        :param mask: Warped Query sub mask
        :param category_id: Warped Query category_id
        :return: similarity top1~5 image list
        """
        db_v, db_name = self.retrieval_db[category_id]["image"], self.retrieval_db[category_id]["filename"]

        if self.hp.extract.model_name.upper() == 'VGG':
            result_images = get_retrieval_result(self.hp, img, mask, db_v, db_name, model=self.model)
        else:
            result_images = get_retrieval_result(self.hp, img, mask, db_v, db_name)

        return result_images


class ImageRetrievalServicerImpl(ImageRetrievalServicer):
    def __init__(self, hp):
        super().__init__()
        self.hp = hp
        self.model = ImageRetrievalModel(self.hp)  # call your model

    def Retrieval(self, input_iterator, context):  # mainly called from client
        try:
            # The format of iterator can be found in client.py.
            # it splits a binary image into chunk_size and yields them.
            input_image = bytearray()
            input_mask = bytearray()
            input_class_id = None

            for input_data in input_iterator:
                if input_data.image is not None:
                    input_image.extend(input_data.image)

                if input_data.mask is not None:
                    input_mask.extend(input_data.mask)

                if input_data.class_id is not None and input_class_id is None:
                    input_class_id = input_data.class_id

            imput_image = bytes(input_image)
            input_mask = bytes(input_mask)

            logging.info(f"Request image: {len(input_image)} bytes")

            input_image = cv2.imdecode(np.frombuffer(imput_image, dtype=np.uint8), cv2.IMREAD_COLOR)  # BGR
            input_mask = cv2.imdecode(np.frombuffer(input_mask, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)

            bitmask = np.zeros_like(input_mask, dtype=int)
            contents_mask = input_mask > 127
            bitmask[contents_mask] = 1
            bitmask = bitmask.astype('float32')

            # Model inference
            retrieval_result_dict = self.model.get_retrieval(img=input_image, mask=bitmask,
                                                                    category_id=input_class_id)

            output_iterator = self._get_binary_iterator(retrieval_result_dict)

            return output_iterator

        except Exception as e:  # logging when an exception occurs
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))
            logging.info(str(e))

    def _get_binary_iterator(self, retrieval_result):
        """
        :param retrieval_result: type dict
            {"name" : Binary_image, "path" : String_path}

        :return: RetrievalResult
        """
        for top_idx, retrieval_item in enumerate(retrieval_result, start=1):
            for idx in range(0, len(retrieval_item["image"]), _CHUNK_SIZE):
                yield RetrievalResult(image_part=retrieval_item["image"][idx:idx + _CHUNK_SIZE],
                                      top=top_idx)
            yield RetrievalResult(path=retrieval_item["path"], top=top_idx)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--log_level", type=str, default="INFO", help="logger level")
    parser.add_argument("-c", "--config", type=str, default='/app/config/sub_retrieval.yaml', help="yaml file for config")
    parser.add_argument("--gpuid", type=str, default="0", help="gpu id")
    parser.add_argument("--port", type=int, default=50300, help="retrieval_grpc port")
    parser.add_argument("--db_path", type=str, default="/app/stylebot_db",
                        help="extract database path")
    parser.add_argument("--model_type", type=str, default="LAB", choices=["HSV", "LAB", "RGB", "VGG"],
                        help="choose model type")

    args = parser.parse_args()

    hp = load_hparam(args.config)

    hp.extract.model_name = args.model_type
    if args.gpuid is not None:
        hp.cuda.device = "cuda:%s" % str(args.gpuid.split(',')[0]) if torch.cuda.is_available() else "cpu"
    else:
        hp.cuda.device = "cuda:%s" % hp.cuda.device if torch.cuda.is_available() else "cpu"

    if args.db_path != "None":
        hp.data.extract = args.db_path

    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=1),
    )

    fashion_retrieval = ImageRetrievalServicerImpl(hp)
    add_ImageRetrievalServicer_to_server(fashion_retrieval, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    print("Server Started >> : {}".format(args.port))
    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()
