import os
import cv2
import torch
import argparse
import numpy as np
import pickle as pkl
from PIL import Image
from tqdm import tqdm

from model.model import Model
from data.transform import get_transform
from utility.utils import load_hparam, get_image_embedding
from submodule.kp_estimator.tools.infer import KPEstimator
from submodule.try_on.clothes_warping import ClothesTryOn

CATEGORY_NUM = 13


class WarpingImage:
    def __init__(self, hp, gpuid, db_root_dir, model_kp_estimator='submodule/kp_estimator/pretrained/final_state.pth'):
        self.hp = hp
        self.db_root_dir = db_root_dir
        self.kp_estimator = KPEstimator(model_kp_estimator, gpuid, args=None)
        self.clothes_warping = ClothesTryOn()
        if self.hp.extract.model_name.upper() == 'VGG':
            self.extract_model = Model(self.hp)

    def get_preprocessed_warpimg(self, image, class_id, mask=None):
        """
        :param image: cv2, RGB Image
        :param class_id: INT, category_id
        :param mask: None
        :return: warped RGB Image
        """
        # Model inference
        keypoints = self.kp_estimator.get_keypoint(image, class_id=class_id)
        clothes_im = np.array(Image.fromarray(image).convert("RGBA").resize((512, 512)))  # RGBA
        warped_img_dict = self.clothes_warping.get_warped_clothes(clothes_im, keypoints, class_id=class_id, mask=mask)

        return warped_img_dict['front']

    def save_db(self):
        if self.hp.extract.model_name.upper() in ['HSV', 'LAB', 'RGB', 'VGG']:
            db_name = "{}_value.pkl".format(self.hp.extract.model_name.lower())

            if not os.path.exists(db_name):
                # database init
                self.retrieval_db = [{"image": None, "filename": None} for _ in range(CATEGORY_NUM)]

                for category_idx in range(CATEGORY_NUM):
                    print("Category : {} / {}".format(category_idx, CATEGORY_NUM-1))
                    category_root_dir = os.path.join(self.db_root_dir, str(category_idx))
                    try:
                        image_list = [os.path.join(category_root_dir, x) for x in os.listdir(category_root_dir)
                            if os.path.splitext(x)[-1].lower() in [".jpg", ".png", ".jpeg"]]
                    except FileNotFoundError as e:
                        print('{}. cannot find database path {}'.format(e, category_root_dir))
                        raise e

                    try:
                        values = []
                        for image_path in tqdm(image_list):
                            _image = cv2.imread(image_path, cv2.IMREAD_COLOR)  # BGR
                            _image = cv2.cvtColor(_image, cv2.COLOR_BGR2RGB)  # RGB

                            # warping
                            _image = self.get_preprocessed_warpimg(_image, class_id=category_idx)  # RGBA
                            _image = cv2.resize(_image, dsize=(self.hp.retrieval.size, self.hp.retrieval.size),
                                                interpolation=cv2.INTER_CUBIC)

                            if self.hp.extract.model_name.upper() == 'HSV':
                                _image = cv2.cvtColor(_image, cv2.COLOR_RGBA2BGR)
                                _image = cv2.cvtColor(_image, cv2.COLOR_BGR2HSV)
                            elif self.hp.extract.model_name.upper() == 'LAB':
                                _image = cv2.cvtColor(_image, cv2.COLOR_RGBA2BGR)
                                _image = cv2.cvtColor(_image, cv2.COLOR_BGR2LAB)
                            elif self.hp.extract.model_name.upper() == 'RGB':
                                _image = cv2.cvtColor(_image, cv2.COLOR_RGBA2RGB)
                            elif self.hp.extract.model_name.upper() == 'VGG':
                                _image = cv2.cvtColor(_image, cv2.COLOR_RGBA2RGB)
                                _image = get_image_embedding(_image, self.extract_model, get_transform(hp, mode='test'))

                            values.append(_image)

                        self.retrieval_db[category_idx]["image"] = np.array(values)
                        self.retrieval_db[category_idx]["filename"] = image_list

                    except IndexError as e:
                        print("ERROR! {}.\nCan not find class_id {} keypoint in submodule/tryon/util/frame_kp".format(e, category_idx))
                        continue

                with open(db_name, "wb") as f:
                    pkl.dump(self.retrieval_db, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", type=str, default='/app/config/sub_retrieval.yaml', help="yaml file for config")
    parser.add_argument("--gpuid", type=str, default="0", help="gpu id")
    parser.add_argument("--db_path", type=str, default="/app/stylebot_db",
                        help="extract database path")
    parser.add_argument("--model_type", type=str, default="LAB", choices=["HSV", "LAB", "RGB", "VGG"],
                        help="choose model type")

    args = parser.parse_args()

    hp = load_hparam(args.config)
    if args.gpuid is not None:
        hp.cuda.device = "cuda:%s" % str(args.gpuid.split(',')[0]) if torch.cuda.is_available() else "cpu"
    else:
        hp.cuda.device = "cuda:%s" % hp.cuda.device if torch.cuda.is_available() else "cpu"
    hp.extract.model_name = args.model_type

    if args.db_path != "None":
        db_root_dir = args.db_path
    else:
        db_root_dir = hp.data.extract

    obj = WarpingImage(hp, args.gpuid, db_root_dir)
    obj.save_db()
