# -*- coding: utf-8 -*-
from __future__ import print_function, division
import cv2
import torch
import numpy as np

from data.transform import get_transform
from utility.utils import convert_db_path, get_converted_image, get_image_embedding


def get_retrieval_result(hp, image, mask, db_v, db_name, model=None):
    """
    main retrieval function
    :param hp: sub_retrieval.yaml
    :param image: warped query image
    :param mask: warped query segmentation mask
    :param db_v: database vectors
    :param db_name: database file names
    :param model: vgg model
    :return: result top 5 dictionary
    """
    print("get_retrieval_result")
    img_feature = get_converted_image(hp, image)  # convert image format
    if model is not None:
        mins, indices = __get_perceptual_similarity(hp, db_v, list(img_feature), mask, model)  # model based 유사도 측정
    else:
        mins, indices = __get_pixel_similarity(hp, db_v, list(img_feature), bitmask=mask)  # pixel based 유사도 측정

    result_images = [{"image": None, "path": None} for _ in range(5)]

    # top (hp.retrieval.topk)
    for i in range(hp.retrieval.topk):
        converted_path, _ = convert_db_path(hp.data.extract, db_name[indices[i]])
        print(converted_path)
        with open(converted_path, "rb") as image:
            result_images[i]["image"] = image.read()
            result_images[i]["path"] = converted_path

    return result_images

def __calculate_hsv_distance(query_feature, db_feature):
    query_h = query_feature[:, :, :, 0]
    query_s = query_feature[:, :, :, 1]
    query_v = query_feature[:, :, :, 2]

    db_h = db_feature[:, :, :, 0]
    db_s = db_feature[:, :, :, 1]
    db_v = db_feature[:, :, :, 2]

    dh = torch.div(torch.min(torch.abs(query_h - db_h), torch.sub(360.0, torch.abs(query_h - db_h))), 180.0)
    ds = torch.abs(query_s - db_s)
    dv = torch.div(torch.abs(query_v - db_v), 255.0)

    distance = torch.mean(torch.sqrt(torch.pow(dh, 2) + torch.pow(ds, 2) + torch.pow(dv, 2)).flatten(start_dim=1),
                           dim=1)

    return distance

def __calculate_rgb_distance(query_feature, db_feature):
    # https://gist.github.com/ryancat/9972419b2a78f329ce3aebb7f1a09152 (java script)

    query_r = query_feature[:, :, :, 0]
    query_g = query_feature[:, :, :, 1]
    query_b = query_feature[:, :, :, 2]

    db_r = db_feature[:, :, :, 0]
    db_g = db_feature[:, :, :, 1]
    db_b = db_feature[:, :, :, 2]

    drp2 = torch.pow(query_r - db_r, 2)
    dgp2 = torch.pow(query_g - db_g, 2)
    dbp2 = torch.pow(query_b - db_b, 2)
    t = (query_r + db_r) / 2

    distance = torch.sqrt(2 * drp2 + 4 * dgp2 + 3 * dbp2 + t * (drp2 - dbp2) / 256.0)
    distance = torch.mean(distance.flatten(start_dim=1), dim=1)

    return distance

def __calculate_lab_distance(query_feature, db_feature):
    # https://gist.github.com/ryancat/9972419b2a78f329ce3aebb7f1a09152 (java script)

    query_l = query_feature[:, :, :, 0]
    query_a = query_feature[:, :, :, 1]
    query_b = query_feature[:, :, :, 2]

    db_l = db_feature[:, :, :, 0]
    db_a = db_feature[:, :, :, 1]
    db_b = db_feature[:, :, :, 2]

    deltaL = query_l - db_l
    deltaA = query_a - db_a
    deltaB = query_b - db_b

    c1 = torch.sqrt(torch.pow(query_a, 2) + torch.pow(query_b, 2))
    c2 = torch.sqrt(torch.pow(db_a, 2) + torch.pow(db_b, 2))

    deltaC = c1 - c2
    deltaH = torch.pow(deltaA, 2) + torch.pow(deltaB, 2) - torch.pow(deltaC, 2)
    deltaH = torch.sqrt(torch.clamp(deltaH, min=0))

    sc = 1.0 + 0.045 * c1
    sh = 1.0 + 0.015 * c1

    deltaLKlsl = torch.div(deltaL, 1.0)
    deltaCkcsc = torch.div(deltaC, sc)
    deltaHkhsh = torch.div(deltaH, sh)
    i = torch.pow(deltaLKlsl, 2) + torch.pow(deltaCkcsc, 2) + torch.pow(deltaHkhsh, 2)

    distance = torch.mean(torch.sqrt(torch.clamp(i, min=0)).flatten(start_dim=1), dim=1)

    return distance

def cie94(query_feature, db_feature):
    """Calculate Lab color difference by using CIE94 formulae

    See http://en.wikipedia.org/wiki/Color_difference or
    http://www.brucelindbloom.com/index.html?Eqn_DeltaE_CIE94.html.

    """

    L1 = query_feature[:, :, :, 0]
    a1 = query_feature[:, :, :, 1]
    b1 = query_feature[:, :, :, 2]

    L2 = db_feature[:, :, :, 0]
    a2 = db_feature[:, :, :, 1]
    b2 = db_feature[:, :, :, 2]

    C1 = torch.sqrt(torch.pow(a1, 2) + torch.pow(b1, 2))
    C2 = torch.sqrt(torch.pow(a2, 2) + torch.pow(b2, 2))

    delta_L = L1 - L2
    delta_C = C1 - C2
    delta_a = a1 - a2
    delta_b = b1 - b2

    delta_H_square = torch.pow(delta_a, 2) + torch.pow(delta_b, 2) - torch.pow(delta_C, 2)

    distance = torch.sqrt(torch.pow(delta_L, 2)
                 + torch.pow(delta_C, 2) / torch.pow(1.0 + 0.045 * C1, 2)
                 + delta_H_square / torch.pow(1.0 + 0.015 * C1, 2))

    distance = torch.mean(distance.flatten(start_dim=1), dim=1)
    return distance

def calculate_percep_loss(anc, pos):
    loss = torch.mean(torch.abs(anc - pos), dim=1)
    return loss

def __get_pixel_similarity(hp, db_feature, query_feature, bitmask=None):
    """
    Model을 RGB/HSV/LAB를 사용했을때, 사용되는 함수

    :param hp: config파일
    :param db_feature: DB item features
    :param query_feature: Anchor query item featrues
    :param bitmask: Ancohr query item mask
    :return: distance, index
    """
    print("get pixel based similarity with {} images".format(hp.extract.model_name.lower()))

    query_feature = np.array(query_feature)
    query_feature = cv2.resize(query_feature, dsize=(hp.retrieval.size, hp.retrieval.size),
                               interpolation=cv2.INTER_AREA)
    if bitmask is not None:
        bitmask = cv2.resize(bitmask, dsize=(query_feature.shape[0], query_feature.shape[1]),
                             interpolation=cv2.INTER_AREA)
    else:
        # bitmask가 없으면, 전체를 보는 것이므로, bitmask를 동일한 size의 value 1 matrix로 만듦
        bitmask = np.ones((query_feature.shape[0], query_feature.shape[1]), dtype=int)

    query_feature = torch.from_numpy(query_feature).type(torch.float32).to(hp.cuda.device)  # (w, h, c)
    db_feature = torch.from_numpy(db_feature).type(torch.float32).to(hp.cuda.device)  # (db items, w, h, c)
    bitmask = torch.from_numpy(bitmask).type(torch.float32).to(hp.cuda.device)  # (db items, w, h, c)

    # matching (query, bitmask) shape to (db_feature) shape.
    query_feature = query_feature[None, :, :, :].repeat(db_feature.shape[0], 1, 1, 1)  # (db_num, w, h, c)
    bitmask = bitmask[None, :, :, None].repeat(db_feature.shape[0], 1, 1, db_feature.shape[3])  # (db_num, w, h, c)

    # apply segmentation mask to image feature
    query_feature *= bitmask
    db_feature *= bitmask

    if hp.extract.model_name.upper() == "HSV":
        _distance = __calculate_hsv_distance(query_feature, db_feature)

    elif hp.extract.model_name.upper() == "LAB":
        # flatten()
        _distance = calculate_percep_loss(
            query_feature.view(query_feature.shape[0], -1),
            db_feature.view(db_feature.shape[0], -1)
        )

    elif hp.extract.model_name.upper() == "RGB":
        _distance = __calculate_rgb_distance(query_feature, db_feature)

    # get top k image and index.
    try:
        mins, indices = torch.topk(_distance, k=hp.retrieval.topk, dim=0, largest=False)
    except RuntimeError:
        # db item이 hp.retrieval.topk 보다 부족할 때
        mins, indices = torch.topk(_distance, k=db_feature.shape[0], dim=0, largest=False)

    return mins, indices


def __get_perceptual_similarity(hp, db_feature, query_feature, bitmask, model):
    """
    Model을 VGG로 사용했을때, 사용되는 함수

    :param hp: config파일
    :param db_feature: DB item features
    :param query_feature: Anchor query item featrues
    :param bitmask: Ancohr query item mask
    :param model: VGG model
    :return: distance, index
    """
    transform = get_transform(hp, mode='test')
    input_image = query_feature * np.tile(bitmask[:, :, None], (1, 1, 3))  # query * (bitmask 1d -> 3d)

    image_feature = get_image_embedding(input_image, model, transform).copy()

    print("get embedding similarity with {} images".format(hp.extract.model_name.lower()))
    # matching (query, bitmask) shape to (db_feature) shape.

    image_feature = torch.from_numpy(image_feature).to(hp.cuda.device)
    image_feature = image_feature[None, :, :].repeat(db_feature.shape[0], 1, 1)
    db_feature = torch.tensor(db_feature).squeeze().to(hp.cuda.device)

    # flatten()
    _distance = calculate_percep_loss(
        image_feature.view(image_feature.shape[0], -1),
        db_feature.view(db_feature.shape[0], -1)
    )

    # get top k image and index.
    try:
        mins, indices = torch.topk(_distance, k=hp.retrieval.topk, dim=0, largest=False)
    except RuntimeError:
        mins, indices = torch.topk(_distance, k=db_feature.shape[0], dim=0, largest=False)

    return mins, indices
