import torch
import torch.nn as nn
import numpy as np
from scipy import linalg as LA
import torchvision.models as models


class Vgg(nn.Module):
    def __init__(self, hp):
        super(Vgg, self).__init__()
        self.hp = hp
        self.vgg16 = models.vgg16(pretrained=False)
        self.vgg16.load_state_dict(torch.load('vgg.pth'))  # deepfashion2로 학습했던 PsNet 전체 검색모델의 weight

        # layer1, 2, 3, 4, 5를 정의
        k = [3, 8, 13, 20, 27]
        self.layers = [
            nn.Sequential(*list(self.vgg16.features.children()))[:k[0]],
            nn.Sequential(*list(self.vgg16.features.children()))[k[0]:k[1]],
            nn.Sequential(*list(self.vgg16.features.children()))[k[1]:k[2]],
            nn.Sequential(*list(self.vgg16.features.children()))[k[2]:k[3]],
            nn.Sequential(*list(self.vgg16.features.children()))[k[3]:k[4]]
        ]

    def forward(self, x):
        x = x.to(self.hp.cuda.device)

        features = []
        for layer in self.layers:
            x = layer(x)
            features.append(torch.squeeze(self._up_scaling(x.cpu()), dim=0).numpy())

        features = self._concat_features(features)
        features = self._to_pca(features)
        return features

    def _concat_features(self, features_list):
        return np.concatenate(features_list, axis=0)

    def _up_scaling(self, feature_map, size=32):
        # feature_map.shape : (512, 14, 14)
        if feature_map.shape[1] > size or feature_map.shape[2] > size:
            # 축소
            return torch.nn.Upsample(size=(size, size), mode='bilinear', align_corners=True)(feature_map)
        else:
            # 확대
            return torch.nn.Upsample(size=(size, size), mode='bicubic', align_corners=False)(feature_map)

    def _to_pca(self, features, n=64):
        # features의 size를 줄이기 위한 pca 함수
        features = np.reshape(features, (features.shape[0], -1))  # (c, h*w)
        norm_x = features - np.mean(features, axis=1, keepdims=True)

        cov = np.cov(features)

        evals, evecs = LA.eigh(cov)
        pca = np.dot(norm_x.T, evecs)
        pca = pca[:, -1:-n - 1:-1]
        return np.moveaxis(pca, 0, -1) # (n, h*w)