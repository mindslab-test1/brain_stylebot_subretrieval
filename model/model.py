from model.zoo import *

class Model:
    def __init__(self, hp):
        self.hp = hp
        self._load_network()

    def _load_network(self):
        self.net = Vgg(self.hp).to(self.hp.cuda.device)

    # This branch Only Extract Vgg
    def extract(self, img):
        self.net.eval()
        with torch.no_grad():
            features = self.net(img.to(self.hp.cuda.device))
        return features
