import pickle as pkl
import sys

from scipy.spatial import Delaunay
import PIL.Image as Image
import numpy as np
import cv2
import os

sys.path.insert(0, 'submodule/try_on')
from triangulate import get_warped_image, denoise_triangulation
from util import get_frame_keypoint, NUM_CLASSES, add_margin_point, to_keypoint

AVATAR_W = 519
AVATAR_H = 1125


class ClothesTryOn:
    def __init__(self):
        self.frame_margin_kp_list, self.frame_kp_list = get_frame_keypoint()

    def get_warped_clothes(self, np_img, clothes_kp, class_id=-1, frame_kp=None, mask=None):
        """
        :param np_img: RGBA cv2 numpy image
        :param clothes_kp: kps model output
        :param class_id: deepfashion2 category_id (0~12)
        :param frame_kp: Stylebot warped Avatar frame keypoints
        :param mask: None (Not use this subretrieval repo.)
        :return: warped image, bbox
        """
        # assertion check
        if not class_id in range(0, NUM_CLASSES):
            raise AssertionError("class_id should be from 0 to {:1d}.".format(NUM_CLASSES - 1))

        if frame_kp is not None:
            frame_kp = to_keypoint(frame_kp)
            frame_kp_margin = add_margin_point(frame_kp)
        else:
            frame_kp = self.frame_kp_list[class_id]
            frame_kp_margin = self.frame_margin_kp_list[class_id]

        h, w = np_img.shape[:2]  # In fact, h = w (decided by segmentation API)
        front_clothes = Image.fromarray(np_img).resize((int(AVATAR_H * (w / float(h))), AVATAR_H))  # RGBA Image
        front_clothes = np.array(front_clothes)

        if mask is not None:
            front_clothes[:, :, 3] = mask

        clothes_kp = clothes_kp / float(h) * AVATAR_H  # adjust both scales of h and w

        clothes_kp_margin = add_margin_point(clothes_kp)
        delaunay_triangles = Delaunay(clothes_kp_margin)

        warped_front, boundary = get_warped_image(clothes_kp_margin, frame_kp_margin, delaunay_triangles.simplices,
                                                  front_clothes)

        # Because we use .png with alpha channel, there is a noise in concatenating triangles on another.
        # To remove this, we apply denoising method.
        denoised_front = denoise_triangulation(warped_front)  # RGBA Image numpy array

        return {'front': denoised_front, 'boundary': boundary}

    def _save_image_with_keypoints(self, image, joints, file_name, width_pad=0):
        ndarr = np.array(image)
        joints_vis = [1 for _ in range(joints.shape[0])]  # not used now

        if width_pad:
            ndarr = ndarr[:, width_pad:-width_pad, :]

        for joint, joint_vis in zip(joints, joints_vis):
            cv2.circle(ndarr, (joint[0], joint[1]), 2, [255, 0, 0], 2)

        cv2.imwrite(file_name, ndarr[:, :, ::-1])
