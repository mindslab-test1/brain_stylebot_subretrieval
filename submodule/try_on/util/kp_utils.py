import numpy as np
import torch


def add_margin_point(landmark, mesh_grid=3):
    min_x, max_x = np.amin(landmark[:, 0]), np.amax(landmark[:, 0])
    margin_x = (max_x - min_x) // 20
    min_y, max_y = np.amin(landmark[:, 1]), np.amax(landmark[:, 1])
    margin_y = (max_y - min_y) // 20

    nx, ny = (mesh_grid, mesh_grid)
    x = np.linspace(min_x - margin_x, max_x + margin_x, nx)
    y = np.linspace(min_y - margin_y, max_y + margin_y, ny)

    for i in range(nx * ny):
        # if i // nx == 0 or i // nx == mesh_grid - 1 or i % ny == 0 or i % ny == mesh_grid - 1:
        if i % ny == 0 or i % ny == mesh_grid - 1:
            landmark = np.concatenate((landmark, np.array([[x[i // nx], y[i % ny]]])), axis=0)

    return landmark


def to_keypoint(frame_keypoint):
    if isinstance(frame_keypoint, tuple) or isinstance(frame_keypoint, list):
        frame_keypoint = np.array(frame_keypoint)

    if isinstance(frame_keypoint, np.ndarray):
        dim = len(frame_keypoint.shape)
        if dim == 2 and frame_keypoint.shape[1] == 2:
            pass
        elif dim == 1:
            print("Reshape 1D array to 2D array")
            frame_keypoint = frame_keypoint.reshape(-1, 2)
        else:
            raise IndexError("frame_keypoint is only supported for (1D or 2D) numpy array."
                             "Given: {:1d}D array".format(dim))
    else:
        raise TypeError("For keypoint, we only supports for tuple, list, and numpy array."
                        "Given: {}".format(type(frame_keypoint)))

    frame_keypoint = frame_keypoint.astype(np.int32)
    return frame_keypoint

def to_psnet_keypoint(frame_keypoint, category_id):
    lm_294 = torch.zeros([294, 2])

    # number of category_landmark
    start_lm_category = [0, 25, 58, 89, 128, 143, 158, 168, 182, 190, 219, 256, 275]
    landmark_start_index = start_lm_category[int(category_id)]
    idx_294 = 0

    for i in range(0, len(frame_keypoint), 2):
        # 2. landmarks to 294 dims torch
        lm_294[landmark_start_index + idx_294][0] = frame_keypoint[i]
        lm_294[landmark_start_index + idx_294][1] = frame_keypoint[i + 1]
        idx_294 += 1

    return lm_294