# -*- coding: utf-8 -*-
import os
import math
import yaml
import time
import copy
import argparse
from tqdm import tqdm
from torch.utils.data import DataLoader
from sklearn.model_selection import train_test_split

from model.model import Model
from utility.writer import Writer
from utility.utils import load_hparam
from data.transform import get_transform
from data.dataloader import RetrievalDataset


def train_loop(hp, model, writer, continue_train=0):
    train_dataset = RetrievalDataset(hp, get_transform(hp, 'train'))
    train_set, val_set = train_test_split(train_dataset, random_state=5436, test_size=0.1)
    train_dataloader = DataLoader(train_set, batch_size=hp.train.batch_size, shuffle=True, num_workers=4)
    val_dataloader = DataLoader(val_set, batch_size=hp.test.batch_size, shuffle=False, num_workers=4)

    print('train dataset size : ', len(train_dataloader))
    print('val dataset size : ', len(val_dataloader))

    train_dataloader = tqdm(train_dataloader)
    val_dataloader = tqdm(val_dataloader)

    since = time.time()
    best_loss = math.inf
    total_epoch = hp.train.epoch
    best_model_wts = None

    for epoch in range(continue_train, total_epoch):
        print('\nEpoch {}/{}\n'.format(epoch, total_epoch - 1), '-' * 10)
        model.epoch = epoch
        # Each epoch has a training and validation phase
        train_epoch_loss = 0.0
        validation_epoch_loss = 0.0

        model.net.train()  # Set model to training mode
        for i, data in enumerate(train_dataloader):
            model.learning(data, writer)
            train_epoch_loss += model.train_loss * hp.train.batch_size
        train_epoch_loss = float(train_epoch_loss) / len(train_dataloader)

        model.net.eval() # Set model to evaluation mode
        for i, data in enumerate(val_dataloader):
            model.inference(data, writer)
            validation_epoch_loss += model.valid_loss * hp.test.batch_size
        validation_epoch_loss = float(validation_epoch_loss) / len(val_dataloader)

        print('Train Loss: {:.4f} Validaion Loss : {:.4f}'.format(train_epoch_loss, validation_epoch_loss))

        if validation_epoch_loss <= best_loss:
            best_loss = validation_epoch_loss
            best_model_wts = copy.deepcopy(model.net.state_dict())

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(
        time_elapsed // 60,
        time_elapsed % 60)
    )

    if hp.log.save_best and best_model_wts is not None:
        model.net.load_state_dict(best_model_wts)
        model.save_checkpoint(best_performance=True)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--name", type=str, default=None,
                        help="name of the model which is used for both logging and saving ckpt")
    parser.add_argument("-c", "--config", type=str, default='./config/sub_retrieval.yaml', help="yaml file for config")
    parser.add_argument("-g", "--gpu_ids", type=str, default=None, help="gpu_ids")
    parser.add_argument("-continue", "--continue_trains", type=int, default=0, help="continue_trains")

    args = parser.parse_args()
    hp = load_hparam(args.config)

    writer = Writer(hp)

    if args.gpu_ids is not None:
        hp.cuda.device = args.gpu_ids.split(',')[0]  # pretend that it uses single gpu

    if args.name is not None:
        hp.log.name = args.name

    hp.log.ckpt_dir = os.path.join(hp.log.ckpt_dir, hp.log.name)
    os.makedirs(hp.log.ckpt_dir, exist_ok=True)

    if hp.data.train_root == "" or hp.data.train_root is None:
        raise Exception("Please specify directories of data in %s" % args.config)

    model = Model(hp)

    print(yaml.dump(hp.to_dict()))
    train_loop(hp, model, writer, args.continue_trains)


if __name__ == '__main__':
    main()
