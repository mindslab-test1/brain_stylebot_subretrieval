import grpc
import argparse
import numpy as np
import cv2
import os
from proto.retrieval_pb2_grpc import ImageRetrievalStub
from proto.retrieval_pb2 import InputItem

_CHUNK_SIZE = 1024*1024


class FashionRetrievalClient(object):
    def __init__(self, remote="0.0.0.0:50300"):  # remote: same with localhost:11000
        channel = grpc.insecure_channel(remote)
        self.stub = ImageRetrievalStub(channel)

    def retrieval(self, image: bytes, mask: bytes, class_id: int):
        iterator = self._binary_iterator(image, mask, class_id)
        return self.stub.Retrieval(iterator)

    @staticmethod
    def _binary_iterator(image, mask, class_id):
        yield InputItem(class_id=class_id)

        for j in range(0, len(image), _CHUNK_SIZE):
            yield InputItem(image=image[j:j+_CHUNK_SIZE])

        for i in range(0, len(mask), _CHUNK_SIZE):
            yield InputItem(mask=mask[i:i+_CHUNK_SIZE])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="fashion retrieval client")

    parser.add_argument(
        "-r",
        "--remote",
        help="grpc: ip:port",
        type=str,
        default="0.0.0.0:50300",
    )

    parser.add_argument(
        "-i",
        "--image",
        help="image path",
        type=str,
        required=True,
    )

    parser.add_argument(
        "-m",
        "--mask",
        nargs="?",
        help="mask path",
        type=str,
        required=True,
    )

    parser.add_argument(
        "-c",
        "--category",
        help="category id",
        type=int,
        required=True,
    )

    parser.add_argument(
        "-o",
        "--output_dir",
        help="output_result_directory",
        type=str,
        default="/app/result"
    )
    print("Client Start")

    args = parser.parse_args()

    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)

    client = FashionRetrievalClient(args.remote)

    with open(args.image, "rb") as i:
        image = i.read()

    with open(args.mask, "rb") as m:
        mask = m.read()

    results = client.retrieval(
        image, mask, args.category
    )

    result_images_dict = {
        '1': {
            'path': None,
            'image': bytearray()
        },
        '2': {
            'path': None,
            'image': bytearray()
        },
        '3': {
            'path': None,
            'image': bytearray()
        },
        '4': {
            'path': None,
            'image': bytearray()
        },
        '5': {
            'path': None,
            'image': bytearray()
        }
    }

    top = 0
    for chunk in results:
        if chunk.top is not None:
            top = chunk.top

        if chunk.image_part is not None:
            result_images_dict[str(top)]['image'].extend(chunk.image_part)

        if chunk.path is not None:
            result_images_dict[str(top)]['path'] = chunk.path

    for idx in result_images_dict:
        top_img = cv2.imdecode(np.fromstring(bytes(result_images_dict[idx]['image']), dtype=np.uint8), cv2.IMREAD_COLOR)
        cv2.imwrite(os.path.join(args.output_dir, "top{}.png".format(idx)), top_img)

    # print top image paths
    print("Downloaded!")