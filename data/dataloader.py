import re
import os
import random
import pickle
import numpy as np
from tqdm import tqdm
from PIL import Image
from pathlib import Path
import torch
from torch.utils.data import Dataset

from update_db import WarpingImage
from data.utils import transform_apply_mode


def image_loader(path, channels=3):
    if channels == 1:
        return Image.open(path).convert('L')  # read gray scale images
    elif channels == 3:
        return Image.open(path).convert("RGB")
    else:
        raise RuntimeError


class RetrievalDataset(torch.utils.data.Dataset):
    """
    학습에만 사용되는 Dataset입니다.
    """
    def __init__(self, hp, transform=None):
        self.hp = hp
        self.root = Path(hp.data.train_root)
        self.warp_obj = WarpingImage(hp, gpuid=hp.cuda.device, db_root_dir=self.root,
                                     model_kp_estimator='submodule/kp_estimator/pretrained/final_state.pth')  # 착장모델
        self.transform = transform
        self.db = {}
        self.data = []

        if not os.path.exists('warped_data.pkl'):
            _category_list = (Path.joinpath(self.root, "db")).glob("*")
            _query_image_list = (Path.joinpath(self.root, "query")).glob("*")
            _mask_image_list = (Path.joinpath(self.root, "mask")).glob("*")

            self.query_image_list = [
                file for file in _query_image_list if file.suffix.lower() in [".jpg", ".png", ".jpeg"]
            ]  # 모든 Ancohr image를 불러옵니다. (전체 착장옷)
            self.mask_image_list = [
                file for file in _mask_image_list if file.suffix.lower() in [".jpg", ".png", ".jpeg"]
            ]  # 모든 mask image를 불러옵니다. (seg mask)

            # _category_list를 int로 정렬합니다. ex) (1, 11, 2) -> (1, 2, 11)
            for category_id, category_path in enumerate(sorted(_category_list, key=lambda x: int(os.path.basename(x)))):
                self.db[str(category_id)] = [
                    file for file in category_path.glob('*') if file.suffix.lower() in [".jpg", ".png", ".jpeg"]
                ]  # category_id별 파일들을 불러옵니다. (Ghost Image)

            # segmask, anchor, positive, negative
            for image_path in tqdm(self.query_image_list):
                masks = re.findall(
                    '{}-.[.]jpg'.format(Path.joinpath(self.root, "mask", image_path.stem)), str(self.mask_image_list)
                )  # Anchor에 해당하는 masks를 불러옵니다.

                for mask in masks:
                    mask_id, mask_category_id = Path(mask).stem.split("-")  # mask의 id와 mask의 category_id를 추출합니다.
                    __db = self.db[mask_category_id].copy()  # 해당 카테고리의 Database를 복사
                    __positive_paths = [
                        db_positive for db_positive in __db if db_positive.stem.split('-')[0] == str(mask_id)
                    ]  # mask의 id와 같은 database item을 positive_path 에 추가합니다.

                    __db = [e for e in __db if e not in __positive_paths]  # positive에 포함된 item을 db에서 제거합니다.
                    for positive_path in __positive_paths:
                        _info = {}

                        # Read image and masks
                        anchor = np.array(image_loader(image_path))  # RGB
                        mask_image = np.array(image_loader(mask, channels=1))
                        positive = np.array(image_loader(positive_path))
                        negative = np.array(image_loader(random.choice(__db)))  # __db에서 랜덤으로 negative 구성

                        # image warping. anchor_warped is 4d image (0~2d:RGB, 3d:Mask)
                        anchor_warped = self.warp_obj.get_preprocessed_warpimg(anchor, int(mask_category_id), mask_image)
                        positive_warped = self.warp_obj.get_preprocessed_warpimg(positive, int(mask_category_id))
                        negative_warped = self.warp_obj.get_preprocessed_warpimg(negative, int(mask_category_id))

                        anchor = anchor_warped[:, :, :3]  # RGB
                        positive = positive_warped[:, :, :3]  # RGB
                        negative = negative_warped[:, :, :3]  # RGB
                        warped_mask = anchor_warped[:, :, 3]  # MASK

                        anchor[warped_mask < 127, :] = 0  # mask없는 부분은 0으로 set

                        _info['anchor'] = Image.fromarray(anchor.astype(np.uint8))
                        _info['positive'] = Image.fromarray(positive.astype(np.uint8))
                        _info['negative'] = Image.fromarray(negative.astype(np.uint8))

                        self.data.append(_info)
                    del __db

            with open('warped_data.pkl', 'wb') as f:
                pickle.dump(self.data, f)

        else:
             with open('warped_data.pkl', 'rb') as f:
                 self.data = pickle.load(f)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        if self.transform is not None:
            _tmp = {}

            anchor = self.data[idx]['anchor']
            positive = self.data[idx]['positive']
            negative = self.data[idx]['negative']

            # config에 horizontal_flip을 설정하면, random augmentation
            if transform_apply_mode(self.hp.preprocess_mode.horizontal_flip, 'train'):
                if random.choice([True, False]):
                    anchor = anchor.transpose(Image.FLIP_LEFT_RIGHT)
                    positive = positive.transpose(Image.FLIP_LEFT_RIGHT)
                    negative = negative.transpose(Image.FLIP_LEFT_RIGHT)

            # 기본적인 transform (norm, resize, etc)
            _tmp['anchor'] = self.transform(anchor)
            _tmp['positive'] = self.transform(positive)
            _tmp['negative'] = self.transform(negative)

            return _tmp

        return self.data[idx]
