import numpy as np
import torchvision.transforms as transforms
from PIL import Image

from data.utils import transform_apply_mode


def get_transform(hp, mode='train'):
    transform_list = []

    if transform_apply_mode(hp.preprocess_mode.resize, mode):
        transform_list.append(transforms.Lambda(__resize))

    transform_list.append(transforms.ToTensor())
    transform_list.append(transforms.Lambda(__normalize))

    return transforms.Compose(transform_list)

def __normalize(img_tensor):
    c, _, _ = img_tensor.shape
    if c == 1:
        return img_tensor
    elif c == 3:
        return transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])(img_tensor)

def __resize(pil_image):
    if len(np.array(pil_image).shape) == 2:
        return transforms.Resize((64, 64), interpolation=Image.BICUBIC)(pil_image)
    elif len(np.array(pil_image).shape) == 3:
        return transforms.Resize((224, 224), interpolation=Image.BICUBIC)(pil_image)