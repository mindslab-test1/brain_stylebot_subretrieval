# Stylebot Sub Retrieval

Author: Jinwoo Kim\
Email : acasia@mindslab.ai

## 모델 설명

input image의 보이는 부분을 갖고, Database속에 비슷한 옷들을 검색해주는 엔진입니다.\
\
검색엔진의 전체 파이프라인은 다음과 같습니다.  
`검색할 착장 image -> segmentation engine (with classification) -> warp engine -> retrieval engine.`\
\
deep learning모델이 아닌, pixel based retrieval 방식입니다.  
image를 LAB채널로 변경후, segmentation mask를 사용하여 부분의 색상값을 구하여 검색합니다.

## 사전 준비

### Pretrained weights
아래 구글 드라이브 링크에서 다운받아, 아래 경로에 넣습니다. 
```
<final_state.pth, pointrend_latest.pth>
https://drive.google.com/drive/folders/1c95F5Uf9kETQrmi2hZb6HrIwaYJQV8gh?usp=sharing
PATH : /app/submodule/kp_estimator/pretrained

<vgg.pth>
https://drive.google.com/drive/folders/1c95F5Uf9kETQrmi2hZb6HrIwaYJQV8gh?usp=sharing
PATH : /app
```

### Update Database
Database의 item을 warping하여 저장합니다.
docker-server의 Entrypoint에서 자동 실행됩니다. (LAB)
``` shell
python update_db.py --model_type LAB
(LAB or HSV or RGB)
```

## Submodule Info
```angular2
submodule/detectron2
-> https://github.com/facebookresearch/detectron2/tree/master/projects/PointRend

submodule/kp_estimator
-> https://github.com/HRNet/HigherHRNet-Human-Pose-Estimation

submodule/try_on
-> stylebot try_on engine
```
## Docker Usage

### Server Docker Registry TAG
`docker.maum.ai:443/brain/stylebot_subretrieval:v1.0.8`

### Docker run
``` bash
docker run -it \
--ipc=host \
--gpus='all' \
-p 50300:50300 \
-v /$DATABASE_PATH:/app/stylebot_db \
--name stylebot_subretrieval docker.maum.ai:443/brain/stylebot_subretrieval:v1.0.8
```

## Server Docker Usage

### Server Docker Registry TAG
`docker.maum.ai:443/brain/stylebot_subretrieval:v1.0.8-server`

### Server Docker RUN Command
```shell script
docker run -itd \
--ipc=host \
--gpus="all" \
-p 50300:50300 \
-v /$DATABASE_PATH:/app/stylebot_db \
--name stylebot_subretrieval docker.maum.ai:443/brain/stylebot_subretrieval:v1.0.8-server
```
