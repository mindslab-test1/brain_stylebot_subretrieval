import os
import cv2
import numpy as np
import yaml
from PIL import Image


class DotDict(dict):
    # https://github.com/mindslab-ai/voicefilter/blob/master/utils/hparams.py#L34
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(self, dict_=None):
        super().__init__()
        if dict_ is not None:
            if not isinstance(dict_, dict):
                raise ValueError
            for k, v in dict_.items():
                if isinstance(v, dict):
                    self[k] = DotDict(v)
                else:
                    self[k] = v

    def to_dict(self):
        output_dict = dict()
        for k, v in self.items():
            if isinstance(v, DotDict):
                output_dict[k] = v.to_dict()
            else:
                output_dict[k] = v
        return output_dict

def get_converted_image(hp, warped_img):
    """
    convert color type

    :param hp: sub_retrieval.yaml
    :param warped_img: image
    :return: converted image
    """
    if hp.extract.model_name.upper() == 'HSV':
        warped_img = cv2.cvtColor(warped_img, cv2.COLOR_BGR2HSV)
        return warped_img
    elif hp.extract.model_name.upper() == 'LAB':
        warped_img = cv2.cvtColor(warped_img, cv2.COLOR_BGR2LAB)
        return warped_img
    elif hp.extract.model_name.upper() == 'RGB':
        warped_img = cv2.cvtColor(warped_img, cv2.COLOR_BGR2RGB)
        return warped_img
    elif hp.extract.model_name.upper() == 'VGG':
        warped_img = cv2.cvtColor(warped_img, cv2.COLOR_BGR2RGB)
        return warped_img
    else:
        raise AssertionError("For now, Only HSV, LAB, RGB, VGG Supported!!!")


def get_image_embedding(warped_img, model, data_transforms):
    """
    :param warped_img: cv2, Wapred RGB Image
    :param model: VGG Model
    :param data_transforms: transform
    :return: Embedding Vectors
    """
    warped_img = Image.fromarray(warped_img.astype(np.uint8))
    warped_img = data_transforms(warped_img).float()
    warped_img = warped_img[None, :, :]
    return model.extract(warped_img)

def load_hparam(filename):
    stream = open(filename, 'r')
    docs = yaml.load_all(stream, Loader=yaml.Loader)
    hparam_dict = DotDict()
    for doc in docs:
        for k, v in doc.items():
            hparam_dict[k] = DotDict(v)
    return hparam_dict

def convert_db_path(db_root_dir, image_path):
    image_postfix = "/".join(image_path.split("/")[-2:])
    return os.path.join(db_root_dir, image_postfix), image_postfix
