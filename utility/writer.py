from torch.utils.tensorboard import SummaryWriter


class Writer(SummaryWriter):
    def __init__(self, hp):
        self.hp = hp
        if self.hp.log.use_tensorboard:
            self.tensorboard = SummaryWriter('./runs/' + self.hp.log.name)

    def train_logging(self, log_name, train_loss, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_scalar('Train/{}'.format(log_name), train_loss, step)

    def test_logging(self, log_name, test_loss, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_scalar('Test/{}'.format(log_name), test_loss, step)

    def train_images_logging(self, img_name, train_imgs, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_images('Train/{}/images'.format(img_name), train_imgs, step)

    def test_images_logging(self, img_name, test_imgs, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_images('Test/{}/images'.format(img_name), test_imgs, step)

    def train_image_logging(self, img_name, train_img, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_image('Train/{}/image'.format(img_name), train_img, step)

    def test_image_logging(self, img_name, test_img, step):
        if self.hp.log.use_tensorboard:
            self.tensorboard.add_image('Test/{}/image'.format(img_name), test_img, step)
